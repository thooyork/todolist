module.exports = {
    plugins: [
      /*
       * automatically adds prefixes (like -moz, -webkit) to CSS files.
       * browserslist is defined in package.json - it should not be defined here after update of
       * autoprefixer to v9.7.3
       */
      require('autoprefixer')({ grid: true })
    ]
  };