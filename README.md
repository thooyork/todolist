# Todo List

> Simple todo list vue.js application with serviceworker offline support and indexedDB support with dexie wrapper

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

### License

This project is under the GNU GPLv3. (GNU General Public License v3.0)

More information here: https://choosealicense.com/licenses/gpl-3.0/




