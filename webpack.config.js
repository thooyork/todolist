const path = require('path');
const webpack = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const AppManifestWebpackPlugin = require('app-manifest-webpack-plugin');
const helper = require('./src/helper');

module.exports = {
  devtool: "source-map",
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    // publicPath: process.env.NODE_ENV === 'production' ? '/' : '',
    filename: 'js/[name].[hash].js'
  },
  module: {
    rules: [
      {
          test: /\.vue$/,
          loader: 'vue-loader'
      },
      {
          test: /\.js$/,
          exclude : /node_modules/,
          use: {
              loader: 'babel-loader',
              options: {
                  presets: ['@babel/preset-env'],
                  plugins: ['@babel/plugin-transform-runtime']
              }
          }
      },
      {
          test: /\.(png|gif|jpg|svg)$/,
          loader: 'file-loader',
          exclude: [/fonts/], // <-we dont want SVG fonts in the image folder
          options: {
            name: "[name].[hash].[ext]",
            outputPath: "images",
            esModule: false,
          }
      },
      {
        test: /\.(woff|woff2|ttf|eot|otf|svg)$/,
        loader: 'file-loader',
        exclude: [/images/], // <-we dont want SVG images in the fonts folder
        options: {
          name: "[name].[hash].[ext]",
          outputPath: "fonts",
        }
      },
      {
          test: /\.(scss|css)$/,
          use: [
          "style-loader",
          "css-loader",
          "postcss-loader",
          "resolve-url-loader",
          "sass-loader"
        ],
      },
      ]
  },
  resolve: { alias: { vue: 'vue/dist/vue.esm.js' } },
  plugins: [
    new VueLoaderPlugin(),
    new ServiceWorkerWebpackPlugin({
      entry: path.join(__dirname, './src/serviceworker.js'),
      filename: 'serviceworker.js',
      publicPath: process.env.NODE_ENV === 'production' ? '/todo/' : '/', //<- Serviceworker Scope
    }),
    new HtmlWebpackPlugin({
      hash: false,
      path: path.resolve(__dirname, './dist'),
      filename: 'index.html',
      template: './src/index_template.html'
    }),
    new webpack.DefinePlugin({
      cacheName: JSON.stringify('sw_cache_todolist_' + helper.uuidv4())
    }),
    new AppManifestWebpackPlugin({
      logo: './src/favicon.svg',
      inject: true,
      emitStats: true,
      output: process.env.NODE_ENV === 'production' ? 'favicons/' : 'favicons/',
      prefix: process.env.NODE_ENV === 'production' ? '/todo/favicons/' : '/favicons/',
      config: {
        appName: 'Simple Todo List',
        appDescription: 'A sample Todo List app to get started with serviceworker and indexedDB.',
        developerName: 'Thomas Haaf',
        developerURL: 'https://www.smart-sign.com',
        background: '#EFEFEF',
        theme_color: '#EFEFEF',
        display: 'standalone',
        orientation: 'portrait',
        start_url: process.env.NODE_ENV === 'production' ? '/todo/index.html' : '/index.html',
        logging: false
      }
    })
  ],
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: false
  }
}
