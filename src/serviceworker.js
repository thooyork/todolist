
// const cacheName = 'CACHE_uuid' defined in webpack config with DefinePlugin

const urlsToCache = () => {
  let addtlUrls;
  addtlUrls = (process.env.NODE_ENV === 'production') ? ['/todo/','/todo/index.html'] : ['/','/index.html'];
  return [].concat(addtlUrls, serviceWorkerOption.assets);
};

const cacheResources = async () => {
  const cache = await caches.open(cacheName);
  return cache.addAll(urlsToCache());
};

self.addEventListener('install', event => {
  self.skipWaiting();
  event.waitUntil(cacheResources());
});

const cachesToKeep = [cacheName]; // <= e.g. ['mycacheKey_89ad3766']

const cleanUpCaches = async () => {
  const keyList = await caches.keys();
  const deletions = keyList.filter(key => cachesToKeep.indexOf(key) === -1).map(key => caches.delete(key));
  for (const success of deletions) {
    return await success;
  }
};

self.addEventListener('activate', async event => {
  await event.waitUntil(cleanUpCaches());
  self.clients.claim();
});

self.addEventListener("fetch", event => {
  event.respondWith(getResponseByRequest(event.request));
});

const getResponseByRequest = async request => {
  try {
    // try to load resource from server
    let response = await fetch(request);
    let cache = await caches.open(cacheName);
    if(!cache.match(request)){
      // if response is not in cache yet, put it there
      cache.put(request, response.clone());
    }
    return response;
  } catch (e) {
    // load resources from cache:
    const cache = await caches.open(cacheName);
    const cachedResponse = await cache.match(request);
    return cachedResponse || new Response("No caches match - prevent error", {"status" : 200, "headers" : {"Content-Type" : "text/plain"}});
  }
};