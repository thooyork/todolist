
import Dexie from 'dexie';
const db = new Dexie('db_todo');
db.version(1).stores({ thingstobedone: '++id, date, description, done'});

export default {
    addTodo: async (description) => {
        db.transaction('rw', db.thingstobedone, async() => {
            let id = await db.thingstobedone.add({date: new Date(), description: description, done: false});
            console.log('Added record ' + id);
        }).catch(e => {
            console.log(e);
        });
    },
    updateTodo: (value) => {
        db.transaction('rw', db.thingstobedone, async() => {
            await db.thingstobedone.update(value.id, {done: value.done, description: value.description, updated: new Date()});
            //console.log('Updated record num. ' + id + ' with value: ' + value);
        }).catch(e => {
            console.log(e);
        });
    },
    getTodos: async () => {
        let todos = await db.thingstobedone.toArray();
        return todos;
    },
    deleteTodo: (id) => {
        db.transaction('rw', db.thingstobedone, async() => {
            await db.thingstobedone.delete(id);
        }).catch(e => {
            console.log(e);
        });
    }

}


