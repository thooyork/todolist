import Vue from 'vue';
import App from './App.vue';
import runtime from 'serviceworker-webpack-plugin/lib/runtime';
import VTooltip from 'v-tooltip';
Vue.use(VTooltip);

new Vue({
  el: '#app',
  render: h => h(App)
});

if ('serviceWorker' in navigator) {
  const registration = runtime.register();
}